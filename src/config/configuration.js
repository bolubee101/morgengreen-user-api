// This should be your own database url
module.exports = {
	database: process.env.DATABASE || "mongodb://127.0.0.1:27017/Morgengreentest",
	jwtsecret: process.env.SECRET || "secret-key-test",
}
