const mongoose = require("mongoose")
const { isEmail } = require("../utils/validator")
const { genSalt, hash } = require('bcrypt')

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
    },
    email: {
        type: String,
        unique: [true, 'Email is already in use'],
        required: [true, 'Email is required'],
        lowercase: true,
        validate: [(value) => { return isEmail(value) }, 'Invalid email']
    },
    phone:{
        type: String,
        unique: [true, 'Phone number is already in use'],
        lowercase: true
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minLength: [8, 'Password cannot be less than 8 characters'],
        maxLength: [20, 'Password cannot be greater than 20 characters']
    },
    confirmed: {
        type: Boolean,
        default: false
    }
},{
    timestamps:true
});

UserSchema.pre('save', async function (next) {
    this.password = await hash(this.password, await genSalt());
    next();
});


exports.User = mongoose.model("User", UserSchema);
