const { generateResponse, createError, createSuccessMessage } = require("../../../utils/response")
const { User } = require("../../../models/users.model")
const { createToken } = require("../../../utils/token")
//const { verifyUser } = require("../../../utils/mail/verifyUser")

exports.register = async function (req, res) {
    let { name, email, phone, password } = req.body
    try {
        const user = await User.create({ name, email: email.toLowerCase(), phone, password })
        if (!user) {
            const result = generateResponse(400, createError({
                message: "Unable to create user",
            }))
            res.status(result.status).json(result.result)
        }
        const token = await createToken({user_id:user._id,email:user.email})
        //verifyUser(email, name, `https://${ req.hostname }/verify/${ token.value }`)
        delete user.password
        const result = generateResponse(201, createSuccessMessage({ user, token: token.value }))
        res.status(result.status).json(result.result)
    } catch (error) {
        const result = generateResponse(400, createError({message:error.message}))
        res.status(result.status).json(result.result)
    }
}
