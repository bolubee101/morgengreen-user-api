const { User } = require("../../../models/users.model")
const { createToken } = require("../../../utils/token")
const { generateResponse, createError, createSuccessMessage } = require("../../../utils/response")
const mailer=require('../../../utils/mail/mailer')
const passwordResetMail=require('../../../utils/mail/PasswordResetMail')

export const forgotPassword = async (req, res) => {
    try {
      const { email } = req.body;
      if (!email) {
        return response(res, false, 400, 'The email field is required');
      }
      const user = await User.findOne({ email: email.toLowerCase() })
      if (!user) {
        const result = generateResponse(400, createError({
            message: "User not found",
        }))
        return res.status(result.status).json(result.result)
      }
      const token = createToken({
        id: user._id,
        email:user.email,
      }, '5m');
       mailer({
        to: email,
        from: 'test@mail.com',
        subject: 'Password Reset',
        html:mailer(passwordResetMail(`${req.hostname}/resetpassword?token=${token}`))
      });
      const result = generateResponse(200, createSuccessMessage({
        message: "resetmail sent successfully"
    }))
    res.status(result.status).json(result.result)
    } catch (err) {
        const result = generateResponse(400, createError({
            message: "There was an error while processing this request",
        }))
        return res.status(result.status).json(result.result)
    }
  };