const { compare } = require("bcrypt")
const { User } = require("../../../models/users.model")
const { generateResponse, createError, createSuccessMessage } = require("../../../utils/response")


module.exports.changePassword = async (req, res) => {
    try {
      const { currentPassword, newPassword } = req.body;
      const { email } = req.user;
      const user = await User.findOne({ email: email.toLowerCase() })
      if (!user) {
        const result = generateResponse(400, createError({
            message: "User not found",
        }))
        return res.status(result.status).json(result.result)
      }
      const comparePassword = compare(currentPassword, user.password);
      if (!comparePassword) {
        const result = generateResponse(400, createError({ message:"Wrong password" }))
        return res.status(result.status).json(result.result)
      }
      user.password = newPassword;
      user.save({validateBeforeSave:false});
      const result = generateResponse(201, createSuccessMessage({ message:"password changed successfully" }))
      return res.status(result.status).json(result.result)
    } catch (err) {

      const result = generateResponse(500, createError({
        message: "something went wrong"
    }))
    res.status(result.status).json(result.result)
    }
  };