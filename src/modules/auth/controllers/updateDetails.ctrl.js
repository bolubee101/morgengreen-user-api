const { compare } = require("bcrypt")
const { User } = require("../../../models/users.model")
const { generateResponse, createError, createSuccessMessage } = require("../../../utils/response")


module.exports.updateDetails = async (req, res) => {
    try {
      req.body;
      const { email } = req.user;
      const user = await User.findOne({ email: email.toLowerCase() })
      if (!user) {
        const result = generateResponse(400, createError({
            message: "User not found",
        }))
        return res.status(result.status).json(result.result)
      }
      const update = User.findOneAndUpdate({email:email.toLowerCase()},   {
        $set: req.body,
      }, {
        new: true,
      })
      const result = generateResponse(201, createSuccessMessage({ message:"update made successfully",update }))
      return res.status(result.status).json(result.result)
    } catch (err) {

      const result = generateResponse(500, createError({
        message: "something went wrong"
    }))
    res.status(result.status).json(result.result)
    }
  };