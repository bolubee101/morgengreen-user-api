const { compare } = require("bcrypt")
const { User } = require("../../../models/users.model")
const { createToken } = require("../../../utils/token")
const { generateResponse, createError, createSuccessMessage } = require("../../../utils/response")

exports.login =  async function (req, res) {
    const { login_id,password } = req.body

    if (!login_id) {
        const result = generateResponse(401, createError({
            message: "Incomplete request"
        }))
        res.status(result.status).json(result.result)
    }

    try {
        // check if all digits
        if(String(Number(login_id))!=="NaN"){
            var user = await User.findOne({ phone: login_id })
            console.log("phone")
        }else{
            var user = await User.findOne({ email: login_id.toLowerCase() })
            console.log("email")
        }
        
        if (user) {
            const auth = await compare(password, user.password)

            if (auth) {
                const token = await createToken({user_id:user._id,email:user.email})
                const result = generateResponse(201, createSuccessMessage({ user, token: token.value }))
                return res.status(result.status).json(result.result)
            } else {
                const result = generateResponse(401, createError({message:"Invalid username or password"}))
                return res.status(result.status).json(result.result)
            }
        } else {
            const result = generateResponse(404, createError({
                message: "User Not Found"
            }))
            return res.status(result.status).json(result.result)
        }
    } catch (error) {
        const result = generateResponse(401, error.message)
            res.status(result.status).json(result.result)
    }
}
