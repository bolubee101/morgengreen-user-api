const User = require("../../../models/User");
const {
  sendFailureResponse,
  sendSuccessResponse,
} = require("../../../utils/appResponse");
const { toHash } = require("../../../utils/password");
const jwtSign = require("../../../utils/token");
const { getFacebookUserData } = require("../utils");

const facebookSignUp = async (req, res) => {
  const { email, first_name, last_name } = getFacebookUserData(req.body.access_token);
  try {
    let user;
    user = await User.findOne({ email });
    if (user) {
      sendFailureResponse(res, 409, "A user with this email already exists.");
      return;
    }
    user = await User.create({
      email,
      first_name,
      last_name,
      full_name: `${first_name} ${last_name}`
    });
    const token = jwtSign(user);
    sendSuccessResponse(res, 201, "User created successfully", {
      user,
      token,
    });
  } catch (error) {
    sendFailureResponse(res, 500, error.message);
  }
};

module.exports = facebookSignUp;
