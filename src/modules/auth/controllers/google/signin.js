const User = require("../../../models/User");
const {
    sendFailureResponse,
    sendSuccessResponse,
  } = require("../../../utils/appResponse");
const jwtSign = require("../../../utils/token");
const { getGoogleUserInfo } = require("../utils");

const googleSignIn = async (req, res) => {
    const { email } = await getGoogleUserInfo(req.body.access_token);
    try {
      const user = await User.findOne({ email });
      if (!user) {
        sendFailureResponse(res, 401, "An account with this email does not exist.");
        return
      }
      if (user.account_type != 'GOOGLE') {
        sendFailureResponse(res, 400, "This account cannot be logged into with Google.");
        return
      }
      const token = jwtSign(user);
      sendSuccessResponse(res, 200, "User logged in successfully", {
        user,
        token,
      });
    } catch (error) {
      sendFailureResponse(res, 500, error.message);
    }
}

module.exports = googleSignIn;
