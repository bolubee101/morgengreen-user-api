const User = require("../../../models/User");
const {
  sendFailureResponse,
  sendSuccessResponse,
} = require("../../../utils/appResponse");
const { toHash } = require("../../../utils/password");
const jwtSign = require("../../../utils/token");
const { getGoogleUserInfo } = require("../utils");

const googleSignUp = async (req, res) => {
  const { email, given_name, family_name } = getGoogleUserInfo(req.body.access_token);
  try {
    let user;
    user = await User.findOne({ email });
    if (user) {
      sendFailureResponse(res, 409, "A user with this email already exists.");
      return;
    }
    user = await User.create({
      email,
      first_name: given_name,
      last_name: family_name,
      full_name: `${given_name} ${family_name}`
    });
    const token = jwtSign(user);
    sendSuccessResponse(res, 201, "User created successfully", {
      user,
      token,
    });
  } catch (error) {
    sendFailureResponse(res, 500, error.message);
  }
};

module.exports = googleSignUp;
