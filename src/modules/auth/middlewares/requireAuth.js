const { createError, generateResponse } = require("../../../utils/response")
const { validateToken } = require("../../../utils/token")
const { User } = require("../../../models/users.model")

exports.requireAuth =  async function (req, res, next) {
    const token = req.header('token')

    if (token) {
        const decodedToken = await validateToken(token)

        if (decodedToken) {
            const user = await User.findById(decodedToken.id)
            req.user = decodedToken
            req.token = token
            next()
        } else {
            const result = generateResponse(403, createError({
                message: "Invalid token"
            }))
            return res.status(result.status).json(result.result)
        }
    } else {
        const result = generateResponse(403, createError({message:"unauthorized access"}))
        return res.status(result.status).json(result.result)
    }
}
