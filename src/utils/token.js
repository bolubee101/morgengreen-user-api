const { sign, verify } = require('jsonwebtoken')
const { Token } = require('../models/token.model')
const { User } = require("../models/users.model")
const {jwtsecret:secret}=require("../config/configuration");
exports.createToken = async function (payload,maxAge=(3 * 24 * 60 * 60)||"") {
    const token = sign(payload, secret, {
        expiresIn: maxAge
    });

    const storedToken = await Token.create({ value: token });

    return storedToken;
}

exports.validateToken = async function (token) {

    try {
        const verifiedToken = verify (token, secret)

        const storedToken = await Token.findOne({ value: token })
        const user = await User.findById(verifiedToken.user_id)
        if (storedToken && user) {
            return verifiedToken
        } else {
            return false
        }
    } catch (error) {
        return false
    }
}


exports.removeToken = async function (token) {
    const removedToken = await Token.deleteOne({ value: token })

    return removedToken;
}
