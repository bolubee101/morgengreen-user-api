const mongoose = require('mongoose')
const config=require('../config/configuration')
/**
 * Starts the application database and connects to it
 */
exports.connectDB = async () => {

    try {
		const conn = await mongoose.connect(config.database, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})
	console.log("DB connected successfully")
	} catch (err) {
		console.log(err.message)
	}
}
