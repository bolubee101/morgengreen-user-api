const express = require('express')
const cors = require('cors')

const PORT = process.env.PORT || 3000
const app = express();
const {connectDB}=require('./utils/connectDb');
connectDB();

const authRoutes = require('./modules/auth/auth.router')



	app.use(express.json({ limit: '10mb' }))
	app.use(express.urlencoded({ extended: true }))
	app.use(cors())

	let pathPrefix = '/api/v1/'

	app.use(`${ pathPrefix }auth`, authRoutes);

	// Handle 404
	app.use('*', (req, res) => {
		res.status(404).json({
			message: "404 | Not Found"
		})
	})

	app.listen(PORT,()=>{
		console.log("server started")
	})
